import Vue from "vue"
import Router from "vue-router"
import Login from "../components/Login";
import MainMenu from "../components/MainMenu"
import Signup from "../components/Signup"
import menuUser from "../components/menuUser"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      component: Login,
    },
    {
      path:"/signup",
      component:Signup
    },
    
    {
      path: "/mainmenu",
      component: MainMenu,
      children: [
        {
          path:'',
          component:menuUser
        },
        {
          path:"userlist",
          component:()=>import('@/components/UserTable')
        },
        {
          path:"userlist/detail",
          name:"userdetail",
          component:()=>import('@/components/UserDetail')
        },
        {
          path:"iorecord",
          component:()=>import('@/components/IORecord')
        },
        {
          path:"codelist",
          component:()=>import('@/components/CodeTable')
        },
        {
          path:"codelist/detail",
          name:"codedetail",
          component:()=>import('@/components/CodeDetail')
        },
        {
          path:"equipmentlist",
          component:()=>import('@/components/EquipmentTable')
        },
        {
          path:"equipmentlist/detail",
          name:"equipmentdetail",
          component:()=>import('@/components/EquipmentDetail')
        },
      ]
    },
    {
      path:"/scan",
      component:()=>import('@/components/Scan')
    },
    {
      path:'*',
      component:()=>import('@/components/404NotFound')
    }
  ]
})
